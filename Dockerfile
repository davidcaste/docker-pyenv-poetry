FROM debian:stable-slim

ARG CI_COMMIT_REF="undefined"
ARG CI_COMMIT_SHA="undefined"

ARG PYTHON_VERSION_37="3.7.9"
ARG PYTHON_VERSION_38="3.8.6"
ARG PYTHON_VERSION_39="3.9.1"
ARG PYTHON_VERSION_310="3.10.5"

LABEL maintainer="davidcaste@gmail.com" \
  ci-commit-ref="${CI_COMMIT_REF}" \
  ci-commit-sha="${CI_COMMIT_SHA}"

ENV PYENV_ROOT="/root/.pyenv"
ENV POETRY_ROOT="/home/foo-user/.poetry"
ENV PATH="/home/foo-user/.local/bin:${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"

RUN echo "Create non-privileged user" && \
  groupadd foo-user && \
  useradd -m -g foo-user -G root -u 1000 foo-user && \
  echo "Install build dependencies" && \
  apt-get update -qq && \
  apt-get install -y -q --no-install-recommends \
    build-essential \
    curl \
    libbz2-dev \
    libffi-dev \
    liblzma-dev \
    libncurses5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libxml2-dev \
    libxmlsec1-dev \
    llvm \
    make \
    tk-dev \
    upx-ucl \
    wget \
    xz-utils \
    zlib1g-dev && \
  echo "Install supported python versions" && \
  apt-get install -y -q git && \
  git clone --depth=1 "https://github.com/pyenv/pyenv.git" "/root/.pyenv" && \
  pyenv install "${PYTHON_VERSION_37}" && \
  pyenv install "${PYTHON_VERSION_38}" && \
  pyenv install "${PYTHON_VERSION_39}" && \
  pyenv install "${PYTHON_VERSION_310}" && \
  pyenv global "${PYTHON_VERSION_310}" && \
  # ugly hack to allow pyenv to be executed with a non-root
  chmod 777 /root && \
  find /root -type d -exec chmod 777 {} \; && \
  find /root -type f -exec chmod +r {} \; && \
  echo "Attempt to remove some unnecessary files" && \
  apt-get autoclean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER 1000

RUN pip3 install --user pre-commit toml-cli && \
  curl -sSL "https://install.python-poetry.org" | python3 - && \
  rm -rf /home/foo-user/.cache && \
  rm -rf /tmp/* /var/tmp/*

WORKDIR /app
